output "private_subnet_ids" {
    description = "List with IDs of the private subnets"
    value = aws_subnet.private_subnet.*.id
}

output "public_subnet_ids" {
    description = "List with IDs of the private subnets"
    value = aws_subnet.public_subnet.*.id
}

output "vpc_id" {
    description = "ID of the VPC"
    value = aws_vpc.vpc.id
}

output "vpc_cidr" {
    description = "ID of the VPC"
    value = aws_vpc.vpc.cidr_block
}


output "public_acl" {
    description = "Public Access Control List"
    value = aws_network_acl.public_acl.id
}

output "private_acl" {
    description = "Private Access Control List"
    value = aws_network_acl.private_acl.id
}
